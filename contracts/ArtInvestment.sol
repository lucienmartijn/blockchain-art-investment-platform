pragma solidity ^0.4.17;// Defines solidity version

// Name of the contract that will be deployed to the ethereum network. 
// You can see it as a class in object oriented progamming.
// This contract/class will be instantiated as an object and is deployed to the network. 
// The contract can be instantiated infinite amount of times, just like a class.
// setting data on the contract costs a bit of ethereum. 
// getting data from the contract is free
contract ArtInvestment{
    
    // modifier is a special keyword where you can define code that will be re-used in multiple functions
    // the _ character is used as a placeholder. The solidity compiler will paste the function code into the _ placeholder while compiling.
    // Each function with this modifier will essentially have the code above the placeholder added to it's function.
    modifier restricted(){
        require(msg.sender==artist._address);
        _;
    }
    
    struct Artist{
        address _address;
        string name;
        string info;
    }
    
    // Properties of the contract. This data is seen as storage data in the contract.
    // To edit this data on the contract, a new ethereum transaction has to occur. This costs a little bit of ether (0.30 USD around this time)
    // address is a datatype which can only hold hexadecimal values like this: 0xf333260F3CeF26623B12989CE7725fd122c14F5a
    // uint is an unsigned integer, which means it can hold up to 2^256 numbers. unsigned means it can only be a positive number
    // boolean is a type which can only hold a true or a false value.
    // [] after a type means it is an array/list of that type. In this case it's length is not defined.
    // mapping can be seen as a hashtable or a dictionary with key value pairs.
    Artist public artist;
    uint public _sharePrice; 
    uint public _sharePriceDecimal;
    uint public _investorAmountShares;
    uint public _artistAmountShares; 
    uint public sharesSold;
    bool public artworkSold;
    
    uint public _contractStartDate;
    uint public _contractEndDate;
    
    address[] public investors;

    mapping(address => uint) investments;
    
    // constructor function is called only once. This will be called when the contract will be instantiated and deployed 
    // to the ethereum network. This consturctor has 3 parameters, which are supplied from the website that instantiates/deploys a contract.
    // msg.sender is a variable which holds an address type value. This value is the address of the sender/payer of the contract.
    constructor(uint sharePrice, uint sharePriceDecimal, uint artistAmountShares, uint investorAmountShares, string name, string info, uint contractStartDate, uint contractEndDate) public{
        artist._address = msg.sender;
        _artistAmountShares = artistAmountShares;
        _sharePrice = sharePrice;
        _sharePriceDecimal = sharePriceDecimal;
        _investorAmountShares = investorAmountShares;
        artist.name = name;
        artist.info = info;
        _contractStartDate = contractStartDate;
        _contractEndDate = contractEndDate;
    }
    
    // invest function lets an investor invest in an artwork.
    // this function has 1 paramter, amount of shares
    // a couple of checks are done with the global function require()
    function invest(uint amountShares) public{ // NOT ETHER, BUT EURO
        require(now < _contractEndDate);
        require(msg.sender!=artist._address);
        require(amountShares <= (_investorAmountShares - sharesSold));
        require(!artworkSold);
        
        uint investment = (_sharePrice + _sharePriceDecimal)*amountShares;
        
        // here the function checks if the caller/sender of this function has already invested into this artwork
        // by checking into the dictionary/hashtable
        // if so, it will update it's total investment in this contract.
        if(investments[msg.sender] > 0) {
            uint newInvestment = investments[msg.sender] + investment;
            investments[msg.sender] = newInvestment;
            // otherwise it will just add it's investment to the dictionary/hashtable for the caller/sender of this function
        } else {
            investments[msg.sender] = investment;
            investors.push(msg.sender);
        }
        // updates the amount of shares sold in this contract
        sharesSold += amountShares;      
    }
    // function checks what your investment is. The caller of this function sends it's ethereum address 
    // to check in the hashtable if this address has an investment
    function getInvestment() public view returns (uint){
        return investments[msg.sender];
    }
    
    // functions returns amount of investors
    function getAmountInvestors() public view returns (uint){
        return investors.length;
    }
    
    // this function will sell the artwork. 
    // It checks if  the caller of this function is the artist via the restricted modifier
    // it also checks if there has been already invested
    function sellArtwork() restricted public{
        require(investors.length > 0); 
        artworkSold = true;
    }
    
    // this function will selfdestruct the contract.
    // It checks if  the caller of this function is the artist via the restricted modifier
    function destroyContract() restricted public{
        selfdestruct(artist._address);
    }
}