import React, { Component } from 'react';
import artInvestments from './artInvestment';
import web3 from './web3';

class ArtContract extends Component {
    state = {
        artist:'',
        shares :0,
        sharesArtist:0,
        sharesSold: 0,
        sharePrice: 0,
        sharePriceDecimal: '',
        startDate:0,
        endDate:0,
        investment: 0,
        amountInvestors : [],
        value : '',
        message : ''
      };

    async componentDidMount(){
        const artist = await this.props.contract.methods.artist().call();
        const amountInvestors = await this.props.contract.methods.getAmountInvestors().call();
        const shares = await this.props.contract.methods._investorAmountShares().call();
        const sharesArtist = await this.props.contract.methods._artistAmountShares().call();
        const sharesSold = await this.props.contract.methods.sharesSold().call();
        const sharePrice = await this.props.contract.methods._sharePrice().call();
        const sharePriceDecimal = await this.props.contract.methods._sharePriceDecimal().call();
        const startDate = await this.props.contract.methods._contractStartDate().call();
        const endDate = await this.props.contract.methods._contractEndDate().call();

        this.setState({
            artist:artist, 
            amountInvestors:amountInvestors,
            shares:shares,
            sharesSold:sharesSold,
            sharePrice:sharePrice,
            sharePriceDecimal: sharePriceDecimal,
            startDate:startDate,
            endDate:endDate
        });
    }

    onSubmit = async (event) =>{
        event.preventDefault();
      
        const accounts = await web3.eth.getAccounts();
        console.log(accounts[0]);
        // if (!accounts.includes('0x60377d0073c9b3b147f79f1a926ff7e50c158b3d')){
        //     this.setState({message: 'For demo purposes only Joey Schouten can invest.'});
        //     return;
        // }

        this.setState({message: 'Waiting on transaction success...'});
      
        await this.props.contract.methods.invest(this.state.value).send({
          from: accounts[0],
        });
      
        this.setState({message: 'You have invested ' + this.state.value + ' shares!'});
      }

    onGetInvestment = async () =>{
        const accounts = await web3.eth.getAccounts();
        
        var investment = await this.props.contract.methods.getInvestment().call({
            from: accounts[0]
        });

        if (this.state.sharePriceDecimal > 0){
            const sharesSold = this.state.sharesSold;
            const sharePriceDecimal = this.state.sharePriceDecimal;
            investment = investment - sharesSold * sharePriceDecimal + sharesSold * (sharePriceDecimal/100);
            console.log(investment);
        }
        
        this.setState({investment: investment});
    }

    timeConverter(unix_timestamp){
        var d = new Date(unix_timestamp * 1000), // Convert the passed timestamp to milliseconds
        yyyy = d.getFullYear(),
        mm = ('0' + (d.getMonth() + 1)).slice(-2),  // Months are zero based. Add leading 0.
        dd = ('0' + d.getDate()).slice(-2);

        // ie: 2014-03-24, 3:00 PM
        return dd + '/' + mm + '/' + yyyy;        
    }
    render() {
        const shares = this.state.shares;
        return (
            <div>
                <h2>{this.props.artName}</h2>          
                <p>
                The owner of this artwork is <strong>{this.state.artist.name}</strong>. <br/><br/>
                Address: {this.state.artist._address} 
                </p> 
                <hr/> 
                <h3>{this.props.artName}</h3>
                <img src={this.props.imgSrc} alt="nice piece of art" width="20%"></img>
                <p style={{width: 50 + '%'}}>
                {this.state.artist.info}  
                </p>   
                
                <hr/>
                <h3>Investment Information</h3>
                <p>Start date: {this.timeConverter(this.state.startDate)}</p>
                <br/>
                <p>End date: {this.timeConverter(this.state.endDate)}</p>
                <br/>
                <p>
                One share is: {this.state.sharePriceDecimal > 0 ? this.state.sharePrice + "," + this.state.sharePriceDecimal : this.state.sharePrice} EUR
                </p>  
                <br/>
                <p>
                Amount of investors: {this.state.amountInvestors}
                </p>
                <br/>
                <p>
                Of the {this.state.shares} shares, {this.state.sharesSold} have been sold.
                </p>
            <hr/>
        
            <form onSubmit={this.onSubmit}>
                <h3>Invest in this artpiece.</h3>
        
                <div>
                <label>Amount of shares to enter: </label>
                <input
                    value ={this.state.value}
                    onChange={event => this.setState({value: event.target.value })}>
                </input> shares
                </div>
                <br/>
                <button>Invest</button>
                </form>
        
            <hr />
        
            <h3>Look up your investment</h3>
            <button onClick={this.onGetInvestment}>Look up</button>
        
            <p> {this.state.investment} EUR invested</p>
            <hr/>
        
            <hr/>
            <h1>{this.state.message}</h1>
          </div>
        );
    }
}

export default ArtContract;
