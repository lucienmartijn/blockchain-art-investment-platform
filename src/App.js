import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import web3 from './web3';
import artInvestments from './artInvestment';
import { Tab, Tabs, TabList, TabPanel } from './react-tabs';
import ArtContract from './ArtContract';

class App extends Component {
  render() {
    if (web3 == null){
      return(
        <div className="bg">
          <div style={{textAlign: "center"}}>
          <img src="logo.svg" alt="vanguart logo" className="landingImg"></img>
            <p>Welcome to the Vanguart pilot page.</p>
            <p>In order to see the smart contracts, please download the MetaMask Chrome extension.</p>
            <p><a href="https://metamask.io/" target="_blank"> Get it here</a></p>
          </div>
        </div>
      );
    } 
    return (
      <div style={{paddingLeft: 15 + 'px'}}>
      <img src="logo.svg" alt="vanguart logo" width="20%" style={{paddingTop: 20 + 'px'}}></img>
      <Tabs>
        <TabList>
          <Tab>Jelmer Konjo</Tab>
          <Tab>Melisa Cola</Tab>
          <Tab>Jochem Walboomers</Tab>
        </TabList>
    
        <TabPanel>
          <ArtContract contract={artInvestments[0]} imgSrc=".\art1.png" artName="Raster with colors 2"/>
        </TabPanel>
        <TabPanel>
          <ArtContract contract={artInvestments[1]} imgSrc=".\art2.png" artName="SATELLITES and NÙDES IV"/>
        </TabPanel>
        <TabPanel>
          <ArtContract contract={artInvestments[2]} imgSrc=".\art3.png"artName="White clouds in blue sky"/>
        </TabPanel>
      </Tabs>
      </div>
    );
  }
}

export default App;
