import Web3 from 'web3';

const web3 = window.web3 == undefined ? null : new Web3(window.web3.currentProvider);

export default web3;