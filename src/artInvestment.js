import web3 from './web3';

const addresses = [
    '0xb243b9FdF62fF01c5799735C7947D851Ca68a715', 
    '0x90cAA6F49F824EF41141EC9f78F323749057eC19',
    '0x098a730a0c7Ad8c63465A074a1efc45E268B748a',
];
//prettier extension where you can reformat json string
const abi= [{"constant":false,"inputs":[],"name":"destroyContract","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"sharesSold","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_contractStartDate","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"amountShares","type":"uint256"}],"name":"invest","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"_sharePriceDecimal","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getAmountInvestors","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"investors","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"artist","outputs":[{"name":"_address","type":"address"},{"name":"name","type":"string"},{"name":"info","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_artistAmountShares","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_sharePrice","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"sellArtwork","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"_investorAmountShares","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getInvestment","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_contractEndDate","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"artworkSold","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"sharePrice","type":"uint256"},{"name":"sharePriceDecimal","type":"uint256"},{"name":"artistAmountShares","type":"uint256"},{"name":"investorAmountShares","type":"uint256"},{"name":"name","type":"string"},{"name":"info","type":"string"},{"name":"contractStartDate","type":"uint256"},{"name":"contractEndDate","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"}];

var contracts;
if (web3 != null){
     contracts = [
        new web3.eth.Contract(abi, addresses[0]), 
        new web3.eth.Contract(abi, addresses[1]), 
        new web3.eth.Contract(abi, addresses[2]),
    ];
}

export default contracts;