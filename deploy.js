const HDWalletProvier = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile'); //interface is ABI

const provider = new HDWalletProvier(
    'keep correct humble episode stage nerve blind seven rubber party evolve note',
    'https://rinkeby.infura.io/1kUus6POEVTscv0aLwQL'
);

const web3 = new Web3(provider);

const deploy = async () =>{
    const accounts = await web3.eth.getAccounts();
    const gasPrice = web3.utils.toWei('2','gwei');

    console.log('Attempting to deploy from account', accounts[0]);

    const result = await new web3.eth.Contract(JSON.parse(interface))
        .deploy({ 
            data: '0x' + bytecode,
            arguments:
            [
                '3', // sharePrice
                '0', // sharePriceDecimal
                '30', // artistAmountShares
                '70', // investorAmountShares
                'Jochem Walboomers', // artist name
                "Jerusalem celebrates, Tel Aviv parties and Gaza bleeds - a surreal 24 hours in which wide range of emotions are triggered. While the Israeli army utilizes high-end technology to expel their enemy, Palestinians throw rocks, burn car tires and use incendiary kites. As humans we see this division and question these events. But how does computer vision interpret this? This is a collage of all the 'White clouds in blue sky' recognised by object recognition software on 14 may 2018 in Jerusalem, Tel Aviv and Gaza.", // Description
                "1531094400", // Start date contract
                "1549670400" //end date contract
            ]
        })
        .send({ gas: 5000000, gasPrice: gasPrice, from: accounts[0] })
        .catch(err => console.log('Contract NOT deployed ', err));

    //console.log(interface);
    console.log('Contract deployed to ', result.options.address );    
};
deploy();