const path = require('path'); // path module
const fs = require('fs'); // file system module
const solc = require('solc');

const artInvestmentPath = path.resolve(__dirname, 'contracts','ArtInvestment.sol');
const source = fs.readFileSync(artInvestmentPath, 'utf8');

console.log(solc.compile(source,1).contracts[':ArtInvestment'].interface);
module.exports = solc.compile(source, 1).contracts[':ArtInvestment'];

